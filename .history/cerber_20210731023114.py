import configparser
import logging

from aiogram import Bot, Dispatcher, executor, types

config = configparser.ConfigParser()
config.read("config.conf")	

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=config[telgram][token])
dp = Dispatcher(bot)

