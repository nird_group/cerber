import configparser
import logging

from aiogram import Bot, Dispatcher, executor, types

config = configparser.ConfigParser()
config.read("config.conf")	

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=config[telegram][token])
dp = Dispatcher(bot)

@dp.message_handler(commands=['me'])
async def send_me(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)