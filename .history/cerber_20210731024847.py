import configparser
import logging

from aiogram import Bot, Dispatcher, executor, types

config = configparser.ConfigParser()
config.read("config.conf")	

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=config["telegram"]["token"])
dp = Dispatcher(bot)

@dp.message_handler(commands=['me'])
async def send_me(message: types.Message):
    me = message.text.split(' ', 1)[1]
    await message.delete()
    await message.answer(me)

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)