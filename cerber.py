import configparser
import logging
import requests
import random

from aiogram import Bot, Dispatcher, executor, types

config = configparser.ConfigParser()
config.read("config.conf")	

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=config["telegram"]["token"])
dp = Dispatcher(bot)

#Обработка команд бота.
@dp.message_handler(commands=['me', 'help', 'пикча'])
async def get_commands(message: types.Message):
    botPrivilege = await bot.get_chat_member(message.chat.id, config["telegram"]["token"])
    msg = message #Сохраняем оригинальное сообщение
    #Проверяем разрешения у бота
    if (botPrivilege["status"]=="administrator" and botPrivilege['can_delete_messages'] == True) : 
        privilegeMessage = ''
        await message.delete() #Удаляем сообщение с командой отправленное пользователем
    else : #Если нет прав добавляем к сообщению предупреждение
        privilegeMessage = '\n\n  _Для корректной работы - сделайте бота администратором и дайте разрешение на удаление и редактирование сообщений!_'
    #Команда /me
    if ("me" in msg.text) :
        me = msg.text.split(' ', 1)[1] #отрезаем саму команду
        await msg.answer("\*\*\**" + msg.from_user.first_name + ' ' + me + '*' + privilegeMessage, parse_mode= 'Markdown')
    #Команда /help
    elif ("help" in msg.text) : 
        await msg.answer('Доступные команды:'
        + '\n /help Эта подсказка' 
        + '\n /me Социалы'
        + '\n /пикча Случайная сиська (18+)'
        + '\n тык Тыкнуть пользователя'
        + '\n Проект на GitLab: https://gitlab.com/volweb/cerber' + privilegeMessage, parse_mode= 'Markdown')
    #Команда пикча
    elif ("пикча" in msg.text) :
        randPick = requests.get('http://api.oboobs.ru/noise/1/')
        randPickId = randPick.json()[0]["id"]
        await msg.answer('Держи ' + msg.from_user.full_name +' http://media.oboobs.ru/noise_preview/' + str(randPickId) + '.jpg')

#Смотрим в текст сообщений
@dp.message_handler(content_types=["text"])
async def get_text(message: types.Message):
    #Команда тык
    if ("тык" in message.text.split(' ', 1)[0] or "Тык" in message.text.split(' ', 1)[0]) : #Влоб но норм )
        userTarget = message.text.split(' ', 2)[1] #Пользавотель назначения
        userSource = message.from_user.full_name #Пользователь инициатор
        random_lines = random.choice(open("user_ttk.txt").readlines()) #Берем случайною строку из файла
        await message.answer(random_lines %{'userTarget': userTarget, 'userSource': userSource}) #Шлем сообщение с подстановкой переменных
        #await message.answer('/me тыкнул ' + user_target)

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)